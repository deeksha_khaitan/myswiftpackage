//
//  MyClass.swift
//  swiftPackageManagerTest
//
//  Created by Deeksha khaitan on 10/12/19.
//  Copyright © 2019 Deeksha khaitan. All rights reserved.
//

import UIKit

public class MyClass: NSObject {
    
   public func doSomething(){
        
       print("doSomethingSpecial")
    }

}
